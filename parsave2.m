%% Parsave is is a special save function that operates within a parloop
function controltwo = parsavetwo(X,TT,Q)
PlateauValue = Q;
save(['Plot_wx' num2str(X) '_wt' num2str(TT) '.mat'],'PlateauValue')
controltwo = 0;
end