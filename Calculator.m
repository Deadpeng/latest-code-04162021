%% Main Calculation branch
function Q = Calculator(Trial,Wave,M,N,cycle,TT,T,golden,pi,J,hbar,X,delta)
    for realization = 1:Trial
        Eigenvec = Wave;
        Mixer = rand(1,1);
        %% Spatial Disorder Setup
        [v,vdiff] = SpatialChaos(M,N,X);
        for step = 1:cycle
       %% Temporal Disorder Setup
            [TP1,TP2,TP3,TP4,TP5] = TemporalChaos(TT,T,step,golden,Mixer,pi);
        %% Time evolution and Velocity Operators     
            UH1 = UnitaryOperator(M,N,4*M*N,2,2,1,1,1,vdiff,v,J,T,TP1,hbar,1);UH2 = UnitaryOperator(M,N,2*N-2,0,1,N-1,0,1,vdiff,v,J,T,TP2,hbar,2);
            UH3 = UnitaryOperator(M,N,N-1,0,1,2*N-1,0,1,vdiff,v,J,T,TP3,hbar,3);UH4 = UnitaryOperator(M,N,N-2,0,1,N-1,0,1,vdiff,v,J,T,TP4,hbar,4);
            UH5 = UnitaryOperator(M,N,0,0,0,0,0,0,vdiff,v,delta,T,TP5,hbar,5);
            V1 = OpticalOperator(M,N,4*M*N,2,2,1,1,1,vdiff,J,T,TP1,hbar,1);V3 = OpticalOperator(M,N,N-1,0,1,2*N-1,0,1,vdiff,J,T,TP3,hbar,3);
        %% Evaluation
        q1 = sum(diag(-(conj(Eigenvec)).'*V1*Eigenvec));   
        Eigenvec = UH2*UH1*Eigenvec;

        q3 = sum(diag(-(conj(Eigenvec)).'*V3*Eigenvec));   
        Eigenvec = UH5*UH4*UH3*Eigenvec;

        q(1,step) = real(q1+q3)/2/N;
        end
        Q(realization,:) = q;
    end