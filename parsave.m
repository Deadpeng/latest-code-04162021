%% Parsave is is a special save function that operates within a parloop
function control = parsave(X,TT,N,Q)
PlateauValue = Q;
save(['L=' num2str(2*N) 'wx' num2str(X) 'wt' num2str(TT) '.mat'],'PlateauValue')
control = 0;
end