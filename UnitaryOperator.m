%% Time Evolution Operator
function Evolver = UnitaryOperator(M,N,mupper,mlower,mincrement,nupper,nlower,nincrement,vdiff,v,J,T,Tchaos,hbar,FloquetStep)
Evolver = zeros(4*M*N,4*M*N); %Introduction to array dimensions to reduce computational cost
if FloquetStep < 5 
for m = mlower:mincrement:mupper
for n = nlower:nincrement:nupper    
    [A,B] = Latticeindex(FloquetStep,N,m,n); %Lattice indexing         
    u = vdiff(A,B);S = sin(sqrt(J^2+u^2)*(T+Tchaos)/5/hbar);C = cos(sqrt(J^2+u^2)*(T+Tchaos)/5/hbar);
    D = sqrt(J^2+u^2);UU = exp(-1i*(v(A)-u)*(T+Tchaos)/5/hbar);
    Evolver(A,B) = 1i*J*S/D*UU;Evolver(B,A) = 1i*J*S/D*UU;
    Evolver(A,A) = (C-1i*u*S/D)*UU;Evolver(B,B) = (C+1i*u*S/D)*UU;                   
end
end
for m = 1:1:2*M*2*N
    if abs(Evolver(m,m)) == 0
        Evolver(m,m) = exp(-1i*v(m)*(T+Tchaos)/5/hbar);
    end
end
else
       Evolver = zeros(2*M*2*N,2*M*2*N);
    for m = 1:(2*M*2*N)
       Evolver(m,m) = exp(-1i*(J*((-1)^(m-1))+v(m))*(T+Tchaos)/5/hbar);
    end
end