%% Velocity Operator
function Solver = OpticalOperator(M,N,mupper,mlower,mincrement,nupper,nlower,nincrement,vdiff,J,T,Tchaos,hbar,FloquetStep)
Solver = zeros(4*M*N,4*M*N);
for m = mlower:mincrement:mupper
for n = nlower:nincrement:nupper    
    [A,B] = Latticeindex(FloquetStep,N,m,n); %Lattice indexing         
    u = vdiff(A,B);
    TU = (T+Tchaos)/5; %Time lapse
    AAA = J*(sin(sqrt(J^2+u^2)*TU/hbar))^2/(J^2+u^2);
    BBB = u*(sin(sqrt(J^2+u^2)*TU/hbar))^2/(J^2+u^2)-1i*(sin(2*sqrt(J^2+u^2)*TU/hbar))/2/sqrt(J^2+u^2);  
    Solver(A,B) = J*BBB;Solver(B,A) = J*conj(BBB);Solver(A,A) = J*AAA;Solver(B,B) = -J*AAA;
end
end