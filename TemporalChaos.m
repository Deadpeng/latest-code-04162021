%% Temporal Disorder 
function [TP1,TP2,TP3,TP4,TP5] = TemporalChaos(TT,T,step,golden,Mixer,pi) 
    TP1 = TT*T*sawtooth(2*pi*(1/5+step-1)*golden+2*pi*Mixer,0.5);
    TP2 = TT*T*sawtooth(2*pi*(2/5+step-1)*golden+2*pi*Mixer,0.5);
    TP3 = TT*T*sawtooth(2*pi*(3/5+step-1)*golden+2*pi*Mixer,0.5);
    TP4 = TT*T*sawtooth(2*pi*(4/5+step-1)*golden+2*pi*Mixer,0.5);
    TP5 = TT*T*sawtooth(2*pi*(5/5+step-1)*golden+2*pi*Mixer,0.5);