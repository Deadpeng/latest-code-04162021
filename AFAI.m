%Modified Anomalous Floquet Anderson Insulator
%Peng Peng Zheng 2020 University of Texas at Dallas
%% Numerical Input
clear;clc;close; %Screen Sweep
hbar = 1; %Planck's constant
J = 1.25; %Tunneling strength
T = hbar*5*pi/2/J; %Period setting
N = 5; %Horizontal Index
M = N; %Vertical Index
delta = 0.4; %Final Step Chemical Potential
golden = 0.5*(1+sqrt(5)); %Introduction of Quasiperiodic Disorder
cycle = 200; %Maximum number of Floquet cycles
Trial = 5; %No of trials performed
%% Wavefunction Setup
W = eye(2*M*2*N);
Wave=[];
for i = 0:(N-1)
    for j = 1:2*N
        Wave = [Wave W(:,(4*N*i+j))];
    end
end
parfor Z = 1:3
for Y = 1:4    
    X = (Y-1)*0.5; %On Site Spatial Disorder strength
    TT = 0.3*Z; %Temporal Disorder strength 
    Q = Calculator(Trial,Wave,M,N,cycle,TT,T,golden,pi,J,hbar,X,delta);
    %% Physical Analysis
    QTest = Q;Q = mean(Q);Qmax = max(Q);index = find(Q==Qmax);indexarray = [index index-1 index-2 index-3 index+1 index+2 index+3];choice = find(indexarray>0);
    PlateauValue = mean(Q(indexarray(choice)));
    %% Data Saving
    control = parsave(X,TT,PlateauValue);
    control = parsave(X,TT,N,QTest);
end
end