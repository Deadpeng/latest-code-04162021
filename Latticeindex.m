%% Lattice Position indices
function [locationA,locationB] = Latticeindex(Step,N,m,n)
if Step==1
    locationA = m;
    locationB = m-1;
elseif Step==2
    locationA = 2+4*N*n+2*(m+1);
    locationB = 1+4*N*rem((n+1),N)+2*m;
elseif Step==3
    locationA = (1+4*N*rem((m+1),N)+2*n);
    locationB = (2+4*N*m+2*n);
elseif Step==4
    locationA = (2+4*N*n+2*m);
    locationB = (1+4*N*n+2*(m+1));
end