%% Spatial Disorder 
function [v,vdiff] = SpatialChaos(M,N,X)
v = 2*X*(rand(1,4*M*N)-0.5);
        vdiff = zeros(2*M*2*N);
        for m = 1:1:(2*M*2*N)
            for n = 1:1:(2*M*2*N)
                vdiff(m,n) = (v(m)-v(n))/2;
            end
        end